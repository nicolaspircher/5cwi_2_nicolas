package tictactoe;
import java.util.Scanner;
public class tictactoe {
	Scanner s = new Scanner(System.in);
	char player = 'X';
	char[][] field = new char[3][3];
	boolean winner = false;
	private String[] getInput() {
		System.out.println("Spieler " + player + " mache eine Eingabe");
		String input = s.next();
		String[] in = input.split(",");
		return in;
	}
	private void init() {
		for (int row = 0; row < field.length; row++) {
			for (int col = 0; col < field.length; col++) {
				field[row][col] = 0;
			}
		}
	}
	private boolean addInputToBoard(String[] input) {
		try {
			int column = Integer.parseInt(input[0]);
			int row = Integer.parseInt(input[1]);
			if (field[row][column] > 0) {
				System.out.println("Feld besetzt!");
				return false;
			} else {
				field[row][column] = player;
				return true;
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Eingabe ungültig!");
			return false;
		} catch (NumberFormatException e) {
			System.out.println("Eingabe ungültig!");
			return false;
		}
	}
	private void changePlayer() {
		if (player == 'X') {
			player = 'O';
		} else if (player == 'O') {
			player = 'X';
		}
	}
	private boolean checkForWinner() {
		boolean theWinner = false;
		for (int row = 0; row <= 2; row++) {
			// check if there are 3 in a row or 3 in a column
			if (field[row][0] == field[row][1] && field[row][1] == field[row][2] && field[row][0] > 0
					|| field[0][row] == field[1][row] && field[1][row] == field[2][row] && field[0][row] > 0) {
				theWinner = true;
			}
		}
		if (field[0][0] == field[1][1] && field[1][1] == field[2][2] && field[0][0] > 0) {
			theWinner = true;
		}
		if (field[0][2] == field[1][1] && field[1][1] == field[2][0] && field[0][2] > 0) {
			theWinner = true;
		}
		return theWinner;
	}
	private void showBoard() {
		System.out.println(" | " + field[0][0] + " | " + field[0][1] + " | " + field[0][2] + " | ");
		System.out.println(" | " + field[1][0] + " | " + field[1][1] + " | " + field[1][2] + " | ");
		System.out.println(" | " + field[2][0] + " | " + field[2][1] + " | " + field[2][2] + " | ");
	}
	public void startGame() {
		init();
		while (!winner) {
			showBoard();
			String[] input = getInput();
			boolean success = addInputToBoard(input);
			if (success) {
				boolean someWinner = checkForWinner();
				if (someWinner) {
					showBoard();
					System.out.println("Spieler: " + player + " hat gewonnen!");
					winner = true;
				}
				changePlayer();
			}
		}
	}
}