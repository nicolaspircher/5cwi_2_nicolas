package c�sar;

public class C�sarEncryption implements Encryptor{
	public int shift;
	public C�sarEncryption(int shift) {
		super();
		this.shift = shift;
		if(this.shift == 0) {
			this.shift = 7;
		}
	}
	@Override
	public String encrypt(String data) {
		// TODO Auto-generated method stub
		String result = "";
		for (int i = 0; i < data.length(); i++) {
			char character = data.charAt(i);
			int ascii = (int) character;
			char shifted = (char) (ascii +shift);
			result += shifted;
		}
		return result;
	}
	@Override
	public String decrypt(String data) {
		String result = "";
		for (int i = 0; i < data.length(); i++) {
			char character = data.charAt(i);
			int ascii = (int) character;
			char shifted = (char) (ascii -shift);	
			result += shifted;
		}
		return result;
	}
}