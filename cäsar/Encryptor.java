package c�sar;

public interface Encryptor {
	public String encrypt(String data);
	public String decrypt(String data);
}
